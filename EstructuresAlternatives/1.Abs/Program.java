/*
 * Program.java        1.0 19/10/2020
 * 
 * Calcula el valor absoluto.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
  /**
   * Calcula el valor absoluto del numero real.
   * 
   * @param -n- un numero real.
   * @return el valor absoluto de -n-.
   */
  public double abs (double n){
    double absoluteValue;
    //Inicializa absoluteValue with the value of -n.
    absoluteValue= n;
    // Si n es negativo, cambia el inicio a absoluteValue.
    if (n < 0 ) {
      absoluteValue = -n;
    }
    // Muestra el valor absoluto.
    return absoluteValue;
}
}