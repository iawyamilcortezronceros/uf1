/*
 * Program.java        1.0 19/10/2020
 * 
 * Calcula el numero maximo entre tres numeros reales.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
  /**
   * Calcula el numero maximo entre tres numeros reales.
   * 
   * @param -num1- un numero real.
   * @param -num2- un numero real.
   * @param -num3- un numero real.
   * @return el valor absoluto de -n-.
   */
  public double screwSize (double midecargol){
    String midetextual;
    //Inicializar las variables.
    //Obtiene el mas grande.
    if (midecargol < 3){
      midetextual = "Petit";
    } else if (midecargol < 5){
      midetextual = "Mitja";
    } else if (midecargol < 6.5){
      midetextual = "Gran";
    } else if (midecargol < 8.5){
      midetextual = "Molt gran";
    }
    System.out.println (midetextual);
  }
}