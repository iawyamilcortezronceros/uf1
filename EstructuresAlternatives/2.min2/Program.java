/*
 * Program.java        1.0 19/10/2020
 * 
 * Calcula el numero minimo.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
  /**
   * Calcula el numero minimo entre dos numeros reales.
   * 
   * @param -n- un numero real.
   * @return el valor absoluto de -n-.
   */
  public double min2 (double n, double m){
    double num1, num2, small;
    //Inicializar las variables.
    num1 = n;
    num2 = m;
    //Obtiene el mas pequeño.
    if (num1 < num2 ) {
      small = num1;
    } else {
      small = num2;
    }

    return small;
}
}