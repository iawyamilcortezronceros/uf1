/*
 * Program.java        1.0 19/10/2020
 * 
 * Calcula el numero maximo.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
  /**
   * Calcula el numero maximo entre dos numeros reales.
   * 
   * @param -n- un numero real.
   * @param -m- un numero real.
   * @return el valor mas grande.
   */
  public double max2 (double n, double m){
    double num1, num2, max;
    //Inicializar las variables.
    num1 = n;
    num2 = m;
    //Obtiene el mas grande.
    if (num1 > num2 ) {
      max = num1;
    } else {
      max = num2;
    }

    return max;
}
}