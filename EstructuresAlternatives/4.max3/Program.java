/*
 * Program.java        1.0 19/10/2020
 * 
 * Calcula el numero maximo entre tres numeros reales.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
  /**
   * Calcula el numero maximo entre tres numeros reales.
   * 
   * @param -num1- un numero real.
   * @param -num2- un numero real.
   * @param -num3- un numero real.
   * @return el valor absoluto de -n-.
   */
  public double max3 (double num1, double num2, double num3){
    double max;
    //Inicializar las variables.
    //Obtiene el mas grande.
    if (num1 < num2 ) {
      if (num2 < num3) {
        max = num3;
      } else {
        max = num2;
      }
    } else if (num1 > num3){
      max = num1;
    } else {
      max = num3;
    }
    return max;
}
}
