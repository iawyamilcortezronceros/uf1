/*
 * Program.java        1.0 19/10/2020
 * 
 * Calcula la raiz quadrada.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;
import java.text.DecimalFormat;
public class Program {
/*
   * @param numero1 es double por que el numero va tener decimales.
   * @param numero2 es double por que el numero va tener decimales.
   * @param numeromostrar es double por que el numero va tener decimales.
   *
   */
    public static void main (String[] args) {
     Scanner scanner = new Scanner(System.in);
     double numero1;
     double numero2;
     
     System.out.print("Donam nombre 1: ");
      numero1 = scanner.nextDouble();
     System.out.print("Doanm nombre 2: ");
     numero2 = scanner.nextDouble();
     
     for ( numero1 = numero1 ; numero1 < numero2 ; numero1 = numero1 + 1 ) {
         
         double numeromostrar = Math.sqrt(numero1);
         // Calcula la raiz cuadrada del numero introducido.
         DecimalFormat df = new DecimalFormat("#.00");
         // Limita la cantidad de decimales que se van a mostrar por pantalla.
      System.out.println("L'arrel quadrade de " + numero1 + " es " + df.format(numeromostrar));
      
     }
    }
}