/*
 * Program.java        1.0 19/10/2020
 * 
 * Muestra la suma de fibonacci hasta donde le marquemos.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;
public class Program {
/*
   * @param numerolimite es int por que se van aintroducir valores enteros.
   * @param suma1 es int por que se van a introducir valores enteros.
   * @param suma2 es int por que se van a introducir valores enteros.
   * @param resultado es int por que se van a introducir valores enteros.
   * @return se mostraran los numeros de fibonacci hasta el límite marcado.
   * 
   */
    public static void main (String[] args) {
     Scanner scanner = new Scanner(System.in);
     int numerolimite, suma1, suma2, resultado;
     
     resultado = 0;
     suma1 = 0;
     suma2 = 1;
     
     System.out.print("Donam nombre: ");
      numerolimite = scanner.nextInt();
      
      while ( resultado <= numerolimite ){
          
          
          System.out.println(resultado);
          
          resultado = suma1 + suma2;
          suma2 = suma1;
          suma1 = resultado;
          
      } 
      
     }
    }