/*
 * Program.java        1.0 19/10/2020
 * 
 * Muestra por fila y después por columna.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class ProgramTUI3 {
  
  public static void main(String[] args) {
   int i, j;
   int[][] a = {{3, 2, 4, 2},{6, 8, 2, 9}};
   System.out.println("Recorregut per files");
   for (i = 0; i < a.length; i++) {
     for (j = 0; j < a.length; j++) {
       System.out.printf("%3d", a[i][j]);
     }
     System.out.printf("\n");
   }
   System.out.println("Recorregut per columnes");
   for (j = 0; j < a.length; j++) {
     for (i = 0; i < a.length; i++) {
       System.out.printf("%3d", a[i][j]);
       System.out.printf("\n");
     }
     System.out.printf("\n");
   }
  }
}