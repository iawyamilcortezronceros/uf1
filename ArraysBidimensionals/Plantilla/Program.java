/*
 * Program.java        1.0 19/10/2020
 * 
 * Crea tabla bidimensional.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;
public class Program {
/*
   * Read and write an array 2d.
   * 
   * 
   * @param args Not used.
   */
    public static void main (String[] args) {
      Scanner s = new Scanner (System.in);
      int nr, nc, i, j;
      int[][] m;
      System.out.printf("\n\nMATRIU\n\n");
      System.out.printf("Nombre de files ?");
      nr = s.nextInt();
      System.out.printf("Nombre de columnas ?");
      nc = s.nextInt();
      m = new int[nr][nc];
      //read the array 2d
      for (i = 0; i < nr; i++) {
        for (j = 0; j < nc; j++) {
          System.out.printf("t[%d][%d] = ", i, j);
          m[i][j] = s.nextInt();
        }
      }
      // write the array 2d ????
        for (i = 0; i < nr; i++) {
          for (j = 0; j < nc; j++) {
            System.out.printf("[%d]", m[i][j]);
        }
          //This is it jump the line. 
         System.out.println();
      }
    }
}