/*
 * Program.java        1.0 19/10/2020
 * 
 * Modelizes the program.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
 /**
  * Agregar tres valores
  * 
  * @param hora sera int porque son valores enteros.
  * @param minuto sera int porque son valores enteros.
  * @param segundo sera int porque son valores enteros.
  * @return se mostrara el porcentaje de descuento.
  */
  public double hms2s(int hora, int minuto, int segundo) {
    // Agregamos los numeros a hora, minuto, segundo.
    int minutoshora;
    int segundofinal;
    
    minutoshora = hora * 60;
    // Con esta operación obtenemos las horas en minutos.
    segundofinal = ((minuto + minutoshora) * 60) + segundo;
    // Con esta operación obtenemos los minutos en segundos.
    return segundofinal;
   
  }
}