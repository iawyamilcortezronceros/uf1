/*
 * Program.java        1.0 19/10/2020
 * 
 * Indica si el numero es par=0 o si es impar=1.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
 /**
  * Agregar un valor
  * 
  * @param numero sera double por los valores decimales.
  * @return se mostrara si es par o impar.
  */
  public double isEven(double numero) {
     return numero % 2;
  }
}