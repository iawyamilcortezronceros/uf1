/*
 * Program.java        1.0 19/10/2020
 * 
 * cambia de metros por segundo a kilometros por hora.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
 /**
  * Agregar dos valores
  * 
  * @param metros sera double por los valores decimales.
  * @return se mostrara los metros y segundos transformados a km/hora.
  */
  public double velocity(double metrosporsegundo) {
    // Agregamos los numeros a metros y segundos.  
    double metrosakm;
    metrosakm = metrosporsegundo * 3.6;
    // Con esta operación obtenemos los km/h.
    return metrosakm;
  }
}