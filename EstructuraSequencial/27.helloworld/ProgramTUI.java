/*
 * Program.java        1.0 19/10/2020
 * 
 * Indica si el numero es par=0 o si es impar=1.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class ProgramTUI {
/*
   * Swap two variales.
   *
   * @param args Not used.
   */
    public static void main(String[] args) {
        System.out.println("Hello world");
        //Se mostrara por pantalla hello world.
        
    }
  
}