/*
 * Program.java        1.0 19/10/2020
 * 
 * Convierte de Fahrenheit a celsius.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
 /**
  * Agregar un numero
  * 
  * @param numero1 sera doubel por que tiene decimales.
  * @return se mostrara el valor convertido a celsius.
  */
  public double fahrenheit2celsius(double fahrenheit) {
    // Agregamos los valores a las variables.
    return (((fahrenheit - 32) * 5 ) / 9);
   
  }
}