/*
 * Program.java        1.0 19/10/2020
 * 
 * Convierto los segundos en horas, minutos y segundos.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;
public class ProgramTUI {
/*
   * Swap 6 variales.
   *
   * @param args Not used.
   */
    public static void main(String[] args) {
        int horas, minutos, segundos, segundostotales, segundos1, minutos1;
        Scanner s = new Scanner(System.in);
        System.out.print("Introduce los segundos : ");
        segundostotales = s.nextInt();
        //Aquí introducimos todos los segundos que queramos convertir.
        minutos1 = segundostotales / 60;
        //Aquí cambiamos los segundos introducidos en minutos pero no son los que mostraremos por pantalla.
        horas = minutos1 / 60;
        //Aquí cambiamos los minutos a horas.
        minutos = minutos1 - (60 * horas);
        //Aquí restamos a los minutos1 los que se requieren para obtener una hora y los restantes serán los minutos que se mostraran por pantalla..
        segundos1 = ((horas * 60) + minutos) * 60;
        //Aquí obtenemos a los segundos1 los que se han requerido para obtener las horas y los minutos.
        segundos = segundostotales - segundos1;
        //Aquí restamos a los segundostotales los segundos1 y el numero restante serán los segundos que mostraremos por pantalla.
        System.out.println( horas + ":" + minutos + ":" + segundos);
    }
  
}