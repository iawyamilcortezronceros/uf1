/*
 * Program.java        1.0 19/10/2020
 * 
 * Suma dos numeros enteros.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
 /**
  * Agregar dos numeros
  * 
  * @param numero1 sera int por que es un valor entero.
  * @param numero2 sera int por que es un valor entero.
  * @return se mostrara la suma de los dos enteros.
  */
  public int add(int numero1, int numero2) {
    // Agregamos los valores a las variables.
    return numero1 + numero2;
   
  }
}