/*
 * Program.java        1.0 19/10/2020
 * 
 * calcula la mediana aritmetica de tres numeros reales.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
 /**
  * Agregar tres numeros
  * 
  * @param numero1 sera int por que es un valor entero.
  * @param numero2 sera int por que es un valor entero.
  * @param numero3 sera int por que es un valor entero.
  * @return se mostrara la media arimetica de los tres enteros.
  */
  public double average(double numero1, double numero2, double numero3) {
    // Agregamos los valores a las variables.
    return (numero1 + numero2 + numero3) / 3;
   
  }
}