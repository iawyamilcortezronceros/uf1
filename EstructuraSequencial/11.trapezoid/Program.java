/*
 * Program.java        1.0 19/10/2020
 * 
 * Modelizes the program.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
 /**
  * Agregar tres valores
  * 
  * @param base1 sera double por los decimales.
  * @param base2 sera double por los decimales.
  * @param altura sera double por los decimales.
  * @return se mostrara el area del trapecio.
  */
  public double area(double base1, double base2, double altura) {
    // Agregamos los numeros a base1, base2, altura.
      
    double resultadofinal;
    
    resultadofinal = ((base1 + base2) * altura) / 2;
    // Con esta operación obtenemos el area del trapecio.
    return resultadofinal;
   
  }
}