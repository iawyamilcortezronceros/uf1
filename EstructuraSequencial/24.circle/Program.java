/*
 * Program.java        1.0 19/10/2020
 * 
 * Modelizes the program.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
 /**
  * Agregar dos numeros
  * 
  * @Parametro base sera double para los decimales.
  * @Paremetro altura sera double para los decimales.
  * @En return se mostrara la base por altura entre dos.
  */
  public double calculadora(double radio, double diametro) {
      
    double area;
    double perimetro;
      area = (radio * radio) * Math.PI;
      perimetro = Math.PI * diametro;

    return perimetro;
   
  }
}