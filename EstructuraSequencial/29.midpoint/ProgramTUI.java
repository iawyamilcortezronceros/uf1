/*
 * Program.java        1.0 19/10/2020
 * 
 * Calcula las coordenadas del punto medio.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;
public class ProgramTUI {
/*
   * Swap 6 variales.
   *
   * @param args Not used.
   */
    public static void main(String[] args) {
        Double cordenada1x, cordenada1y, cordenada2x, cordenada2y, m1, m2;
        Scanner s = new Scanner(System.in);
        System.out.print("Introduce cordenada 1a: ");
        cordenada1x = s.nextDouble();
        System.out.print("Introduce cordenada 1b: ");
        cordenada1y = s.nextDouble();
        System.out.print("Introduce cordenada 2a: ");
        cordenada2x = s.nextDouble();
        System.out.print("Introduce cordenada 2b: ");
        cordenada2y = s.nextDouble();
        m1 = (cordenada1x + cordenada2x) / 2;
        m2 = (cordenada1y + cordenada2y) / 2;
        System.out.print("(" + m1 + ":" + m2 + ")");
    }
  
}