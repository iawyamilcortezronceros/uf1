/*
 * Program.java        1.0 19/10/2020
 * 
 * Modelizes the program.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
 /**
  * Agregar dos numeros
  * 
  * @param preciotarifa sera double para los decimales.
  * @param preciopagado sera double para los decimales.
  * @return se mostrara el porcentaje de descuento.
  */
  public double discount(double preciotarifa, double preciopagado) {
    // Agregamos los numeros a preciotarifa y preciopagado.
    double preciodescuento;
    double porcentajedescuento;
    preciodescuento = (preciotarifa - preciopagado);
    // Con esta operación obtenemos la cantidad descontada.
    porcentajedescuento = (preciodescuento * 100) /preciopagado;
    // Con esta operación obtenemos la cantidad descontada en porcentaje.
    return porcentajedescuento;
   
  }
}