/*
 * Program.java        1.0 19/10/2020
 * 
 * Se cambian los valores que sean introducido en las variables.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;
public class ProgramTUI {
/*
   * Swap tres variables.
   *
   * @param args Not used.
   */
    public static void main(String[] args) {
        int numero1;
        int numero2;
        int cambio;
        Scanner s = new Scanner(System.in);
        System.out.println("Introduce un numero para numero1:");
        //Se mostrara por pantalla.
        numero1 = s.nextInt();
        System.out.println("Introduce un numero para numero2:");
        //Se mostra por pantalla
        numero2 = s.nextInt();
        cambio = numero1;
        numero1 = numero2;
        numero2 = cambio;
        System.out.println("El valor de numero1 es "+ numero1 +" el valor de numero2 es "+ numero2);
        //Se mostrara por pantalla.
        
    }
  
}