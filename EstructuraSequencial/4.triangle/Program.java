/*
 * Program.java        1.0 19/10/2020
 * 
 * Calcula el area de un triangulo.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
 /**
  * Agregar dos numeros
  * 
  * @param numero1 sera double por que tiene decimales.
  * @param numero2 sera doble por que tiene decimales.
  * @return se mostrara el area del triangulo.
  */
  public double area(double base, double altura) {
    // Agregamos los valores a las variables.
    return (base * altura) / 2;
   
  }
}