/*
 * Program.java        1.0 19/10/2020
 * 
 * Modelizes the program.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
 /**
  * Agregar un valor
  * 
  * @param temperatura sera double por los decimales.
  * @return se mostrara la temperatura en fahrenheit.
  */
  public double celsius2fahrenheit(double temperatura) {
    // Agregamos los numeros a hora, minuto, segundo.
      
    double resultadofinal;
    
    resultadofinal = (temperatura * 9 / 5) + 32;
    // Con esta operación obtenemos los celcius en fahrenheit.
    return resultadofinal;
   
  }
}