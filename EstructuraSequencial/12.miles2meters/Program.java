/*
 * Program.java        1.0 19/10/2020
 * 
 * Modelizes the program.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
 /**
  * Agregar un valor
  * 
  * @param milla sera double por los valores decimales.
  * @return se mostrara la milla en metros.
  */
  public double miles2meters(double milla) {
    // Agregamos los numeros milla.
      
    double resultadofinal;
    
    resultadofinal = (milla * 1852);
    // Con esta operación obtenemos las millas en metros.
    return resultadofinal;
   
  }
}