/*
 * Program.java        1.0 19/10/2020
 * 
 * Muestra los valores introducidos en una lista.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;

public class ProgramTUI3 {
    /*
   * @param n es int por que se van a introducir valores enteros.
   * @param a es double por que se van a introducir valores decimales.
   * @param i es int por que se van a introducir valores enteros.
   * @return se mostraran los valores introducidos en la lista.
   * 
   */
 
    public static void main(String[] args) {
     Scanner s = new Scanner(System.in);
     System.out.print("Lectura i escriptura de una raiz de numeros\n\n");
     System.out.print("Dimensió?");
     // Pide la cantidad de valores que vamos a introducir en la lista.
     int n = s.nextInt();
     double[] a = new double[n];
     for (int i = 0; i < n; i++) {
         System.out.printf("a[%d]= ", i);
         // Se muestra la posicion en la que el valor va ir
         a[i] = s.nextDouble();
         
     }
     System.out.print("\nArray :\n");
     for (int i = 0; i < a.length; i++) {
         System.out.printf("a [%d] = %f\n", i,a[i]);
         // Se muestran todos los valores introducidos en la lista
     }
     
    }
    
}