/* Program.java        1.0 19/10/2020
 * 
 * Muestra los valores introducidos en una lista.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;

public class ProgramTUI3 {
    /*
   * @param n es int por que se van a introducir valores enteros.
   * @param a es double por que se van a introducir valores decimales.
   * @param i es int por que se van a introducir valores enteros.
   * @param sum es double por que se van a introducir valores decimales.
   * @return se mostraran los valores introducidos en la lista.
   * 
   */
 
    public static void main(String[] args) {
     Scanner s = new Scanner(System.in);
     System.out.print("Lectura i escriptura de una raiz de numeros\n\n");
     // Reade the lebght of the array
     System.out.print("Dimensió?");
     int n = s.nextInt();
     // Create the array, allocate enough memory for n elements
     double[] a = new double[n]; // 0,1,2,3,..... n
     // Read n numbers and store them in the array
     for (int i = 0; i < n; i++) {
         System.out.printf("a[%d]= ", i);
         a[i] = s.nextDouble();
         
     }
     System.out.print("\nArray :\n");
     Double sum;
     sum = 0.0;
     for (int i = 0; i < a.length; i++) {
         sum = a[i] + sum;
         System.out.println("a " + a[i]);
         System.out.println("SUMA : " + sum);
     }
     
    }
    
}