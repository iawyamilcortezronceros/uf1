/*
 * Program.java        1.0 19/10/2020
 * 
 * Muestra el numero mas grande.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;

public class ProgramTUI3 {
    /*
   * @param numerolimite es int por que se van aintroducir valores enteros.
   * @param suma1 es int por que se van a introducir valores enteros.
   * @param suma2 es int por que se van a introducir valores enteros.
   * @param resultado es int por que se van a introducir valores enteros.
   * @return se mostrara el numero mas grande de la lista.
   * 
   */
 
    public static void main(String[] args) {
     Scanner s = new Scanner(System.in);
     System.out.print("Lectura i escriptura de una raiz de numeros\n\n");
     // Reade the lebght of the array
     System.out.print("Dimensió?");
     int n = s.nextInt();
     // Create the array, allocate enough memory for n elements
     int[] a = new int[n]; // 0,1,2,3,..... n
     // Read n numbers and store them in the array
     for (int i = 0; i < n; i++) {
         System.out.printf("a[%d]= ", i);
         a[i] = s.nextInt();
         
     }
     int mayor = a[0];
     for (int i = 1; i < a.length; i++) {
         if (a[i] > mayor) {
             mayor = a[i];
         }
  
     }
     System.out.println("el numero mas grande es :" + mayor);
}
}