/*
 * Program.java        1.0 19/10/2020
 * 
 * Muestra el cuadrado y el cubo de los valores introducidos .
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;

public class ProgramTUI3 {
    /*
   * @param cuadrado es int por que se van aintroducir valores enteros.
   * @param cubo es int por que se van a introducir valores enteros.
   * @return se mostraran los numeros al cuadrado y al cubo.
   * 
   */
 
    public static void main(String[] args) {
     Scanner s = new Scanner(System.in);
     System.out.println("Calcula la raíz cuadrada y el cubo de los valores introducidos");
     // Introducimos la cantidad de valores que van a ir en la lista.
     System.out.print("Cuantos valores vas a introducir?");
     int n = s.nextInt();
     int[] a = new int[n];
     // Introducimos los valores a la lista.
     for (int i = 0; i < n; i++) {
         System.out.printf("a[%d]= ", i);
         a[i] = s.nextInt();
         
     }
     // Calculamos la raíz cuadrada.
     int cuadrado;
     System.out.println("Cuadrado:");
     for (int i = 0; i < a.length; i++) {
         cuadrado = a[i] * a[i];
         System.out.println(a[i] + " = " + cuadrado);
     }
     // Calculamos el cubo.
     int cubo;
     System.out.println("Cubo:");
     for (int i = 0; i < a.length; i++) {
         cubo = a[i] * a[i] * a[i];
         System.out.println(a[i] + " = " + cubo);
     }
    }
    
}