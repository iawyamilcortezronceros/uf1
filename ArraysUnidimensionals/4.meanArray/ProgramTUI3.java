/*
 * Program.java        1.0 19/10/2020
 * 
 * calcula la media de los valores introducidos.
 * 
 * Copyright 2020 Yamil Cortez Ronceros <cortezyamil123321@gmail.com>
 * 
 * this is free software, licensed under the GNU General Public License v.3
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;

public class ProgramTUI3 {
    /*
   * @param suma es double por que los valores contienen decimales.
   * @param resultado es double por que los valores contienen decimales.
   * @return se mostrara la media de los numeros introducidos.
   * 
   */
 
    public static void main(String[] args) {
     Scanner s = new Scanner(System.in);
     System.out.print("Calcular media de los numeros introducidos\n\n");
     System.out.print("Cuantos valores va introducir?");
     int n = s.nextInt();
     int[] a = new int[n];
     //Aquí establecemos los valores que van a ver en la lista.
     
    for (int i = 0; i < n; i++) {
         System.out.printf("a[%d]= ", i);
         a[i] = s.nextInt();
    }
    //Aqui hemos introducido los valores a la lista.
    double suma = 0;
    double resultado = 0;
    for (int i= 0; i < a.length; i++) {
        suma = suma + a[i];
    }
    //Aquí hemos realizado la suma de todas las variables que hay en la lista.
    
    resultado = suma / n;
    System.out.println("La media es : " + resultado);
}
}